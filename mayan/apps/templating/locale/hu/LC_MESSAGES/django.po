# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# molnars <szabolcs.molnar@gmail.com>, 2024
# Csaba Tarjányi, 2024
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-07 07:29+0000\n"
"PO-Revision-Date: 2024-05-07 07:30+0000\n"
"Last-Translator: Csaba Tarjányi, 2024\n"
"Language-Team: Hungarian (https://app.transifex.com/rosarior/teams/13584/hu/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hu\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:21 permissions.py:6
msgid "Templating"
msgstr "Sablonozás"

#: apps.py:39
msgid "Current date and time"
msgstr ""

#: fields.py:28
#, python-format
msgid ""
"Use Django's default templating language "
"(https://docs.djangoproject.com/en/%(django_version)s/ref/templates/builtins/)."
" "
msgstr ""
"A Django alapértelmezett sablonozó nyelvének használata "
"(https://docs.djangoproject.com/en/%(django_version)s/ref/templates/builtins/)."
" "

#: fields.py:43
#, python-format
msgid "Available template context variables: %s"
msgstr ""

#: forms.py:9
msgid "Resulting text from the evaluated template."
msgstr "Eredmény szöveg a kiértékelt sablonból."

#: forms.py:10
msgid "Result"
msgstr "Eredmény"

#: forms.py:20
msgid "The template string to be evaluated."
msgstr ""

#: forms.py:21
msgid "Template"
msgstr "Sablon"

#: links.py:10
msgid "Sandbox"
msgstr ""

#: permissions.py:10
msgid "Use the template sandbox"
msgstr "Sandbox használata"

#: serializers.py:8
msgid "Hex hash"
msgstr ""

#: serializers.py:11
msgid "Name"
msgstr "Név"

#: serializers.py:14
msgid "HTML"
msgstr ""

#: serializers.py:17
msgid "URL"
msgstr "URL"

#: views.py:45
#, python-format
msgid "Template sandbox for: %s"
msgstr ""

#: views.py:67
#, python-format
msgid "Template error; %(exception)s"
msgstr ""

#: widgets.py:69
msgid "Filters"
msgstr ""

#: widgets.py:76
msgid "Tags"
msgstr "Jelölők"

#: widgets.py:83
msgid "<Filters and tags>"
msgstr "<Filters and tags>"

#: widgets.py:128
msgid "<Model attributes>"
msgstr "<Model attributes>"
